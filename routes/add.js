var express = require('express');
var router = express.Router();

/* POST Add. */
router.post('/', function(req, res, next) {
    let { a, b } = req.body;
    a = parseInt(a);
    b = parseInt(b);
    result =  a + b;
  res.status(200).json(result);
});

/* GET Add. */
router.get('/', function(req, res, next) {
  res.render('add', { title: 'Calculator: Add' });
});

module.exports = router;