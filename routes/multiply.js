var express = require('express');
var router = express.Router();

/* POST Multiply. */
router.post('/', function(req, res, next) {
    let { a, b } = req.body;
    a = parseInt(a);
    b = parseInt(b);
    result =  a * b;
  res.status(200).json(result);
});

/* GET Multiply. */
router.get('/', function(req, res, next) {
  res.render('multiply', { title: 'Calculator: Multiply' });
});

module.exports = router;